@extends('layout.master')

@section('judul')
    Home
@endsection

@section('content')
    <h1>SanberBook</h1>
    <h2>Social Media Developer Santai Berkualitas</h2>
    <p>Belajar dan Berbagi agar hidup semakin santai berkualitas</p>

    <h3>Benefit Join di Sanberbook</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowledge dari para mastah sanber</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h3>Cara Bergabung ke sanberBook</h3>
    <ol>
        <li>Mengunjungi website ini</li>
        <li>Mendaftar di <a href="/form">form Sign Up</a></li>
        <li>Selesai!</li>
    </ol>
@endsection
