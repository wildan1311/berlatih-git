<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <form action="/kirim" method="POST">
        @csrf
        <h1>Buat Account Baru</h1>
        <h3>Sign Up Form</h3>
        <label for="first_name">First Name : <br></label><br>
        <input type="text" name="first_name" id=""> <br><br>
        <label for="last_name">Last Name : <br></label><br>
        <input type="text" name="lastname" id=""> <br><br>
        <label for="gender">Gender : <br></label><br>
        <input type="radio" name="gender" id="" value="Male"> Male <br>
        <input type="radio" name="gender" id="" value="Female"> Female <br>
        <input type="radio" name="gender" id="" value="Other"> Other <br><br>
    
        <label for="nasionality">Nasionality</label> <br><br>
        <select name="nasionality" id="">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapore">Singapore</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>
    
        <label for="spoken">Language Spoken</label><br><br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>
    
        <label for="box">Box</label><br><br>
        <textarea name="box" id="" cols="30" rows="10"></textarea><br><br>
        <input type="submit" name="submit" id="submit">
    </form>
</body>
</html>
