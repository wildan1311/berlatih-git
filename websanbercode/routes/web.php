<?php

use App\Http\Controllers\kirimController;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/form', function () {
    return view('form');
});

Route::post('/kirim', [kirimController::class , 'kirim']);

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/table', function(){
    return view('content.table');
});

Route::get('/data-table', function(){
    return view('content.data-table');
});