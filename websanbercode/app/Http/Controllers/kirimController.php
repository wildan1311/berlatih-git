<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class kirimController extends Controller
{
    public function kirim(Request $request){
        $data = [
            "first_name" => $request['first_name'],
            "lastname" => $request['lastname']
        ];

        return view('/welcome', $data);
    }
}
