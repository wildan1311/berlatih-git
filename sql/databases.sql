create database myshop;

create table users(
    id integer auto_increment primary key,
    name varchar(255),
    email varchar(255),
    password varchar(255)
);

create table categories(
    id integer auto_increment primary key,
    name varchar(255)
);

create table items(
    id integer auto_increment primary key,
    name varchar(255),
    description varchar(255),
    price integer,
    stock integer,
    category_id integer,
    foreign key(category_id) references categories(id)
);

insert into users (name, email, password) 
values
(
    "John Dae", "john@doe.com", "john123"
),
(
    "Jane Dae", "jane@doe.com", "jenita123"
);

insert into categories(name)
values
(
    "gadget"
),
(
    "cloth"
),
(
    "men"
),
(
    "women"
),
(
    "branded"
);

insert into items(name, description, price, stock, category_id)
values
(
    "Sumsang b50","hape keren dari merek sumsang",4000000,100,1
),
(
    "Uniklooh","baju keren dari brand ternama",500000,50,2
),
(
    "IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1
);

select name, email
from users;

select * 
from items
where price > 1000000;

select * 
from items
where name like 'uniklo%';

select i.name, i.description, i.price, i.stock, i.category_id, categories.name
from items i join categories
on category_id = categories.id;

update items
set price=2500000
where name='sumsang b50';